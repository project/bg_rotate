<?php

use Drupal\file\Entity\File;

/**
 * @file
 * Install, update, and uninstall functions for the Background rotate module.
 */

/**
 * Implements hook_uninstall().
 */
function bg_rotate_uninstall() {
  $file_usage = \Drupal::service('file.usage');
  // Get bg_rotate managed file ids.
  $fids = \Drupal::database()->select('file_usage', 'fu')
    ->condition('fu.module', 'bg_rotate', '')
    ->fields('fu', ['fid'])
    ->execute()
    ->fetchCol();
  // Load files and remove bg_rotate reference for deletion.
  $files = File::loadMultiple($fids);
  foreach ($files as $file) {
    $file_usage->delete($file, 'bg_rotate');
  }
  // Destroy state.
  \Drupal::state()->delete('bg_rotate.images');
}

/**
 * Implements hook_update_N().
 */

/**
 * Migrate old settings.
 */
function bg_rotate_update_8001() {
  // Check for old images settings.
  if (\Drupal::config('bg_rotate.settings')->get('images')) {
    $images = \Drupal::config('bg_rotate.settings')->get('images');
    \Drupal::config('bg_rotate.settings')->delete('images');
  }
  if (\Drupal::state()->get('images')) {
    $images = \Drupal::state()->get('images');
    \Drupal::state()->delete('images');
  }
  if ($images && !\Drupal::state()->get('bg_rotate.images')) {
    \Drupal::state()->set('bg_rotate.images', $images);
  }
  // Check for old breakpoint settings.
  if (
    \Drupal::config('bg_rotate.settings')->get('screen') &&
    \Drupal::config('bg_rotate.settings')->get('tablet') &&
    \Drupal::config('bg_rotate.settings')->get('phone') &&
    !\Drupal::config('bg_rotate.settings')->get('breakpoints')
  ) {
    $config = \Drupal::configFactory()->getEditable('bg_rotate.settings');
    $breakpoints = [
      [
        'image_style' => $config->get('screen'),
        'width' => 1600,
      ],
      [
        'image_style' => $config->get('tablet'),
        'width' => 900,
      ],
      [
        'image_style' => $config->get('phone'),
        'width' => 640,
      ],
    ];
    $config->set('breakpoints', $breakpoints)->save();
    $config->clear('screen');
    $config->clear('tablet');
    $config->clear('phone');
  }
}
